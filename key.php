<?php
    include 'core/session.php';
    include 'core/database.php';
    include 'core/logged.php';

$q="SELECT
(SELECT count(*) FROM veille WHERE keyword='Web' GROUP BY keyword) AS Web,
(SELECT count(*) FROM veille WHERE keyword='Hacks' GROUP BY keyword) AS Hack,
(SELECT count(*) FROM veille WHERE keyword='Logiciel' GROUP BY keyword) AS Logiciel,
(SELECT count(*) FROM veille WHERE keyword='Autres' GROUP BY keyword) AS Autre,
(SELECT count(*) FROM veille WHERE keyword='Objets connecté' GROUP BY keyword) AS IOT,
(SELECT count(*) FROM veille WHERE keyword='Robotique' GROUP BY keyword) AS Robot";
$promo=$_POST['promo'];
   if(isset($promo) && $promo!="All") {
$q="SELECT
(SELECT count(*) FROM veille INNER JOIN users u ON veille.id_user=u.id 
 WHERE keyword='Web' && u.promo='$promo' GROUP BY keyword) AS Web,
(SELECT count(*) FROM veille INNER JOIN users u ON veille.id_user=u.id
 WHERE keyword='Hacks' && u.promo='$promo' GROUP BY keyword) AS Hack,
(SELECT count(*) FROM veille INNER JOIN users u ON veille.id_user=u.id
 WHERE keyword='Logiciel' && u.promo='$promo' GROUP BY keyword) AS Logiciel,
(SELECT count(*) FROM veille INNER JOIN users u ON veille.id_user=u.id
 WHERE keyword='Autres' && u.promo='$promo' GROUP BY keyword) AS Autre,
(SELECT count(*) FROM veille INNER JOIN users u ON veille.id_user=u.id
 WHERE keyword='Objets connecté' && u.promo='$promo' GROUP BY keyword) AS IOT,
(SELECT count(*) FROM veille INNER JOIN users u ON veille.id_user=u.id
 WHERE keyword='Robotique' && u.promo='$promo' GROUP BY keyword) AS Robot";
   }
$stats=mysqli_fetch_assoc(mysqli_query($handle,$q));
 ?>
<!doctype html>
<html>
<head>
  <title>Catégories</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="template/style.css">
  <style>
     .row {
       margin-right: 0;
       }
  </style>
  <script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script type="text/javascript">

window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer", {
		title:{
			text: "Catégories <?php echo $promo; ?>"
		},
		data: [
		{
   		type: "doughnut",
			dataPoints: [
	      	<?php 
	      	// For each type ($key => line) of veille when number > 0, print it in the chart
	      	foreach($stats as $key => $line)
               echo ($line>0)?'{label:"'.$key.'",y:'.$line.'},':'';			
		      ?>
			]
		}
		]
	});
	chart.render();
}
</script>
<?php include('template/header.php'); ?>
  <div class="container-fluid">
    <div class="col-xs-8 col-xs-offset-2">
    <div class="row">
      <h3>Catégories  </h3>
</div>
      <div class="row categorie">
        <div id="form">
          <form class="key" action="key.php" method="post">
            <div class="col-xs-4">
               <select name="keyword" tabindex="1" class="form-control">
                 <option value="0"> - - </option>
                 <option value="Web">Web</option>
                 <option value="Objet connecté">Objets connectés</option>
                 <option value="Logiciel">Logiciel</option>
                 <option value="Hacks">Hack</option>
                 <option value="Robotique">Robotique</option>
                 <option value="Autre">Autre</option>
               </select>
            </div>
            <div class="col-xs-4">
                  <select name="promo" tabindex="1" class="form-control">
                    <option value="All">All</option>
                    <option value="Ada Lovelace">Ada Lovelace</option>
                    <option value="Alan Turing">Alan Turing</option>
                    <option value="Autre">Autres</option>         
                  </select>
               </div>
            <div class=" col-xs-2">
              <input type="submit" tabindex="2" name="submit" class="btn btn-info" value="Sélectionner">
            </div>
            <div class="col-xs-2 " id="cancel">
              <input type="submit" tabindex="3" name="cancel" class="btn btn-info" value="Reset">
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
    <div class="row ">
      <div class=" col-xs-8 col-xs-offset-2">
        <?php
          $keyword=$_POST['keyword'];
        if(isset($_POST['submit']) && $promo!="All" && $keyword!='0') {
          $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille INNER JOIN users u ON veille.id_user=u.id 
 WHERE keyword='$keyword' && u.promo='$promo'";
 
        } elseif(isset($_POST['submit']) && $promo=="All" && $keyword!='0') {
          $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille WHERE keyword='$keyword'";  
          
        } elseif(isset($_POST['cancel'])) {
          header('Location:key.php');
          
        } else {
          $query = "SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee FROM veille";
        }
        if(isset($_POST['submit']) && $promo!="All" && $keyword==0)
               $query.=" INNER JOIN users u ON veille.id_user=u.id WHERE promo='$promo'";
               
        $result = mysqli_query($handle,$query);

        if($handle->affected_rows > 0) {
          while($line=mysqli_fetch_array($result)) {
            $i--;

            echo "\t\t<div id='veille_membre'>\n";
            echo "\t\t\t<div class='row'>\n";
            echo "\t\t\t\t<div class='col-xs-2'>\n";
            echo "\t\t\t\t\t<p class='dark'>" .$line['date_formatee']."</p>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-7'>\n";
            echo "\t\t\t\t\t<div class='subject'>\n";
            $title = $line['subject'];
            if(strlen($title) > 60){
              $title = substr($title, 0, 60) ."...";
            }
            echo "\t\t\t\t\t\t<p class='titre'> <a href='veille.php?id=".$line['id']."'><img class='sujet_img' src='img/sujet.png'>   " . $title." </a></p>\n";
            echo "\t\t\t\t\t\t<p> <img class='key_img' src='img/key.png'> ".$line['keyword']."</p>\n";
            echo "\t\t\t\t\t</div>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-2 col-xs-offset-1'>\n";
            echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='img/go.png'></a>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t</div>\n";
            echo "\t\t</div>\n";
          }
        } else {
          echo "\t\t\t<p>Aucune veille n'a été postée dans cette catégorie pour le moment...</p>\n";
        }
        ?>
      </div>
    </div>
    <div class="col-xs-offset-4" id="chartContainer" style="height: 300px; width: 30%;"></div>
  </div>
<?php include ('template/footer.php'); ?>
