<?php
    include 'core/session.php';
    include 'core/database.php';
    include 'core/logged.php';
 ?>
<!doctype html>
<html>
<head>
	<title>Popschoolers</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="template/style.css">
	<style media="screen">
		img {
			max-width: 54px;
			height: 54px;
			border: 1px #02AFEF solid;
			border-radius: 2px;
         box-shadow: 3px 3px 2px silver;
         padding: 2px;
		}
      select {
         margin-bottom:20px;
      }
	</style>
<?php include('template/header.php'); ?>
<div class="container">
  <div class="row">
		<div class="col-xs-12 ">
         <h3>Les Popschoolers</h3>
      </div>
            <form action="name.php" method="POST">
               <div class="col-xs-10">
                  <select name="promo" tabindex="1" class="form-control">
                    <option value="All">All</option>
                    <option value="Ada Lovelace">Ada Lovelace</option>
                    <option value="Alan Turing">Alan Turing</option>
                    <option value="Autre">Autres</option>         
                  </select>
               </div>
               <div class=" col-xs-2">
                  <input type="submit" tabindex="2" name="submit" class="btn btn-info" value="Sélectionner">
               </div>
            </form>
<?php
   $promo=$_POST['promo'];
   $query="SELECT * FROM users";
   if(isset($promo) && $promo!="All") {
      $query.=" WHERE promo='$promo'";
   }
   $result=mysqli_query($handle,$query);
   $i=0;
   while($line=mysqli_fetch_array($result)) {
		 $i++;
		 echo "\t\t\t<a href='membre.php?id=" . $line['id'] . "'>\n";
		 echo "\t\t\t<div class='col-xs-12 col-md-4'>\n";
		 echo "\t\t\t\t<div class='row'>\n";
		 echo "\t\t\t\t\t<div id='popschoolers'>\n";
		 echo "\t\t\t\t\t\t<div class='col-xs-1'>\n";
		 echo "\t\t\t\t\t\t\t<p class='num'>" .$i."</p>\n";
		 echo "\t\t\t\t\t\t</div>\n";
		 echo "\t\t\t\t\t\t<div class='col-xs-3 '>\n";
		 echo "\t\t\t\t\t\t\t<img  src='uploads/".$line["img"]."'>\n";
		 echo "\t\t\t\t\t\t</div>\n";
		 echo "\t\t\t\t\t\t<div class='name'>\n";
		 echo "\t\t\t\t\t\t\t<div class='col-xs-7'>\n";
		 echo "\t\t\t\t\t\t\t\t<h4 class='nom'>".ucfirst(strtolower($line['firstname']))." ".ucfirst(strtolower($line['name']))."</h4>\n";
		 echo "\t\t\t\t\t\t\t</div>\n";
		 echo "\t\t\t\t\t\t</div>\n";
		 echo "\t\t\t\t\t</div>\n";
		 echo "\t\t\t\t</div>\n";
		 echo "\t\t\t</div>\n";
		 echo "\t\t\t</a>\n";
   }

?>
</div>
         </div>
      </div>
   </div>
<?php include ('template/footer.php'); ?>
