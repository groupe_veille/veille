-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 09, 2016 at 12:08 PM
-- Server version: 5.6.30-1
-- PHP Version: 5.6.22-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `veilleApp`
--

-- --------------------------------------------------------

--
-- Table structure for table `badge`
--

CREATE TABLE `badge` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `boss` int(11) NOT NULL DEFAULT '0',
  `veille` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `badge`
--

INSERT INTO `badge` (`id`, `id_user`, `boss`, `veille`) VALUES
(1, 2, 1, 0),
(2, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `message` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `username`, `message`) VALUES
(1, 'Leos', 'Hey!');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_veille` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `id_user`, `id_veille`, `content`, `date`) VALUES
(4, 2, 3, 'Ta veille pue du cul', '2016-08-14 17:22:00'),
(5, 2, 3, 'Non mais vraiment', '2016-08-14 17:36:00'),
(6, 2, 3, '1/20', '2016-08-14 17:38:00'),
(7, 2, 3, '!!!', '2016-08-14 17:49:00'),
(8, -1, 53, 'Sympa ta veille AmÃ©lie continue comme ca !', '2016-09-05 01:34:00');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `id_veille` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `id_veille`, `nom`, `type`, `description`) VALUES
(1, 1, 'phpnth0Wv.odt', 'odt', 'Ceci est un test'),
(52, 52, 'phpk84Yzi.jpg', 'jpg', ''),
(53, 53, 'phpzVJBsX.png', 'png', 'Coucou comment ca va ?'),
(54, 54, 'phpp75ibD.txt', 'txt', ''),
(55, 55, 'phpsPpyXY.txt', 'txt', 'coucou toi ca va ?'),
(56, 56, 'phpcclpN5.jpg', 'jpg', 'Hey'),
(59, 59, 'no', '', 'Hello'),
(63, 63, 'phps843RV.jpg', 'jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `popularity`
--

CREATE TABLE `popularity` (
  `id_user` int(11) NOT NULL,
  `id_veille` int(11) NOT NULL,
  `vote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `popularity`
--

INSERT INTO `popularity` (`id_user`, `id_veille`, `vote`) VALUES
(1, 1, 1),
(3, 1, 1),
(2, 3, 1),
(0, 3, 1),
(2, 48, 1),
(2, 51, 1),
(2, 18, 1),
(2, 20, 1),
(4, 3, 1),
(2, 53, 1),
(1, 57, 1),
(-1, 56, 1);

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE `promos` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `typectn`
--

CREATE TABLE `typectn` (
  `id` int(11) NOT NULL,
  `id_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `promo` varchar(50) NOT NULL,
  `img` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `firstname`, `username`, `password`, `promo`, `img`) VALUES
(-1, 'School', 'POP', 'admin', 'f4a5b2bba0865c88535f6a2b6417610fcf34498f', 'Autre', 'phpfi7JII.jpg'),
(1, 'Delory', 'AmÃ©lie', 'AD', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(2, 'Riem', 'Leo', 'Leos', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'phpdNlbZo.jpg'),
(3, 'Voeux', 'Mathilde', 'Math', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(4, 'Riem', 'Benjamin', 'betaqv', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(6, 'Cliquet', 'AurÃ©lie', 'ohdearzigzag', 'ca55c4c7a3e21543d37807eff30213bfacef6a8f', 'Alan Turing', 'default-thumbnail.png'),
(7, '9999', '9999', '9999', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Ada Lovelace', 'default-thumbnail.png'),
(8, 'Leuve', 'Lave', 'Love', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Ada Lovelace', 'default-thumbnail.png'),
(12, 'Annehiem', 'Jean-Pierre', 'jp', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(13, 'Surquin', 'Jimmylan', 'jimmy', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(14, 'Piton', 'Ingrid', 'ip', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(15, 'Buirette', 'Pierre', 'pb', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(16, 'Bross', 'Victor', 'vb', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(17, 'Farkh', 'Selim', 'sf', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Alan Turing', 'default-thumbnail.png'),
(19, 'Doe', 'John', 'jd', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Ada Lovelace', 'default-thumbnail.png');

-- --------------------------------------------------------

--
-- Table structure for table `veille`
--

CREATE TABLE `veille` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `subject` varchar(255) NOT NULL,
  `keyword` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `selected` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `veille`
--

INSERT INTO `veille` (`id`, `id_user`, `date`, `subject`, `keyword`, `title`, `selected`) VALUES
(1, 2, '2016-06-06 00:00:00', 'VR', 'Autres', 'Hololens', 0),
(2, 4, '2016-06-12 00:00:00', 'Moteur de recherche', 'Web', 'Qwant', 0),
(3, 1, '2016-08-05 00:00:00', 'Robot qui fait la cuisine', 'Robotique', 'Robot cuisto', 0),
(46, 3, '2016-08-12 16:58:00', 'Test', 'Web', 'Tests', 0),
(51, 1, '2016-08-14 02:14:00', 'Sujet', 'Web', 'Titre', 0),
(52, 2, '2016-08-15 14:48:00', 'Un truc intÃ©ressant', 'Robotique', 'Best veille EUW', 0),
(53, 1, '2016-08-15 20:22:00', 'Crash test nÂ°35', 'Autres', 'Encore un titre qui va faire un carton', 1),
(55, 2, '2016-08-15 21:25:00', '123456', 'Web', '456', 0),
(56, 19, '2016-08-16 17:00:00', 'Test', 'Hacks', 'Testing', 0),
(57, 2, '2016-09-01 20:36:00', 'Qt Creator', 'Logiciel', 'IDE', 0),
(58, 7, '2016-09-01 21:40:00', 'Montre qui donne l\'heure', 'Objets connectÃ©', 'Techno de pointe', 0),
(59, 7, '2016-09-01 21:42:00', 'Voiture qui roule', 'Objets connectÃ©', 'BestCarEver', 0),
(63, 3, '2016-09-02 00:42:00', 'Moteur de recherche very performant', 'Web', 'Yahoo!', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `badge`
--
ALTER TABLE `badge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typectn`
--
ALTER TABLE `typectn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `veille`
--
ALTER TABLE `veille`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `badge`
--
ALTER TABLE `badge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `promos`
--
ALTER TABLE `promos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `typectn`
--
ALTER TABLE `typectn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `veille`
--
ALTER TABLE `veille`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
