<?php
    include 'core/session.php';
    include 'core/database.php';
    include 'core/logged.php';
?>
<!doctype html>
<html>
<head>
   <title>Page d'accueil</title>
   <meta charset>
   <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="template/style.css">
   <style media="screen">

	   img {
		   max-width:60px;
		   height: 60px;
		   border-radius: 2px;
		   margin-bottom: 4px;
		   float: left;
		   margin-right: 15px;
		   background-color: #F0F6C6;
		   padding: 3px;
		   border-radius: 2px;
		   border: 1px #00AEEF solid;
       box-shadow: 2px 2px 2px silver;
	   }
   </style>
<?php include 'template/header.php'; ?>

   <div class="container">
      <div class="row">
        <div class="col-md-4 col-xs-5">
          <div class="rss">

          <script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');</script>
          <script type="text/javascript">(function() {var params = {rssmikle_url: "http://web.developpez.com/index/rss",rssmikle_frame_width: "100%",rssmikle_frame_height: "400",frame_height_by_article: "0",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif", rssmikle_font_size: "12",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "off",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "20",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "Développez.com",rssmikle_title_link: "http://www.developpez.com/",rssmikle_title_bgcolor: "#FFFFFF",rssmikle_title_color: "#37474F",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#00acc1",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M %p",item_description_style: "text",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();</script>
        <script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');</script><script type="text/javascript">(function() {var params = {rssmikle_url: "http://www.alsacreations.com/rss/actualites.xml",rssmikle_frame_width: "100%",rssmikle_frame_height: "300",frame_height_by_article: "0",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "12",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "off",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "30",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "Alsacréations",rssmikle_title_link: "",rssmikle_title_bgcolor: "#FFFFFF",rssmikle_title_color: "#37474F",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#00ACC1",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M %p",item_description_style: "text",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();</script>
        <!-- start feedwind code --><script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');</script><script type="text/javascript">(function() {var params = {rssmikle_url: "http://www.lemonde.fr/pixels/rss_full.xml",rssmikle_frame_width: "100%",rssmikle_frame_height: "400",frame_height_by_article: "0",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "12",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "off",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "20",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "Le Monde - Informatique",rssmikle_title_link: "",rssmikle_title_bgcolor: "#FFFFFF",rssmikle_title_color: "#37474F",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#00ACC1",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M %p",item_description_style: "text",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();</script>
      </div>
      </div>
         <div class="col-md-8 col-xs-7">

            <div class="bonjour">

<?php
                  echo "\t\t\t\t<h3>BONJOUR <span class='pseudo'>".$username."</span></h3>\n";
                  include 'include/date.php';
                  echo "\t\t\t\tLES VEILLES DU JOUR : <span class='dark'>".$dday." ".$jour." ".$month." ".$annee."</span><br>\n";
?>
            </div>





<?php
		$query="SELECT * FROM ( SELECT * FROM veille ORDER BY date desc ) v GROUP BY v.id_user";
		$result=mysqli_query($handle,$query);

		while($line=mysqli_fetch_array($result)) {
      $id_user=$line['id_user'];
      $id_veille=$line['id'];
      ?>



<?php
      echo "\t\t<div class='col-md-4 col-xs-12'>\n";
		echo "\t\t\t<a href='membre.php?id=".$line['id_user']."'>\n";
      echo "\t\t\t<div id='veille'>\n";

      $query="SELECT * FROM users WHERE id='$id_user'";
      $user_r=mysqli_query($handle,$query);
      $line_user=mysqli_fetch_array($user_r);

      $query="SELECT *, DATE_FORMAT(veille.date, '%d/%m à %H:%i') as heure FROM veille WHERE id='$id_veille'";
      $veille_r=mysqli_query($handle,$query);
      $line_veille=mysqli_fetch_array($veille_r);
			echo "\t\t\t\t<div class='id'>\n";
			echo "\t\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t\t<div class='col-md-4'>\n";
			echo "\t\t\t\t\t\t\t<img  src='uploads/".$line_user["img"]."'>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t\t<div class='col-md-8'>\n";
			echo "\t\t\t\t\t\t\t<h4 class='nom'>".ucfirst(strtolower($line_user['firstname']))." ".ucfirst(strtolower($line_user['name']))."</h4>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
      echo "</a>";
      echo "\t\t\t<a href='veille.php?id=".$line['id']."'>\n";
			echo "\t\t\t\t<div class='subject'>\n";
			echo "\t\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t\t<div class='col-md-3 col-xs-2'>\n";
			echo "\t\t\t\t\t\t\t<p class='sujet'><span class='titre'>SUJET  </span></p>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t\t<div class='col-md-9 col-xs-10'>\n";

			$title = $line['title'];
			if(strlen($title) > 60){
				$title = substr($title, 0, 60) . "...";
			}
			echo "\t\t\t\t\t\t\t".ucfirst(strtolower($title))."\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t\t<div class='row'>\n";
			echo "\t\t\t\t\t<div class='col-md-12'>\n";
			echo "\t\t\t\t\t\t<div class='heure'>\n";
			//$date = date('H:i:s', strtotime($line_veille['date']));
			echo "\t\t\t\t\t\t\t<p class='dark'>Postée le ". $line_veille['heure'] ."</p>\n";
			echo "\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t</div>\n";
			echo "\t\t\t\t</div>\n";
			echo "\t\t\t</div>\n";
			echo "\t\t\t</a>\n";
			echo "\t\t</div>\n";

	}
?>
</div>


 </div>
 </div>




<?php
include ('template/footer.php') ?>
