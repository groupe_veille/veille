<?php
if ($id) {
  if(isset($_GET['id']) && $id==-1) {
    $permitted=1;
    $id=$_GET["id"];
  }
  elseif(!isset($_GET['id']) && $id==-1) {
    $permitted=1;
  }
  elseif (isset($_GET["id"]) && $_GET["id"] != $id) {
    $permitted=0;
    $id=$_GET["id"];
  }
  elseif (isset($_GET["id"]) && $_GET["id"]==$id) {
    $permitted=1;
  }
  else {
    $permitted=1;
  }
} else {
  if (isset($_GET['id'])) {
    $permitted=0;
    $id=$_GET["id"];
  } else {
    header('Location:login.php');
  }
}
?>
