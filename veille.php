<?php
include ('core/session.php');
include ('core/database.php');
include ('core/permission.php');
include ('core/logged.php');
?>
<!doctype html>
<html>
<head>
  <title>Veille</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="template/style.css">
  <!-- Go to www.addthis.com/dashboard to customize your tools -->

  <style media="screen">
  </style>
<?php include('template/header.php') ?>

  <div class="container">

    <div class='row'>
      <div class='col-md-10 col-md-offset-1'>
        <?php
        if (isset($_GET['id'])) {
          //Les requêtes
          $id_veille=$_GET['id'];
          $query="SELECT *, DATE_FORMAT(date, '%d/%m à %H:%i') as date_formatee FROM veille WHERE id='$id_veille'";
          $line=mysqli_fetch_array(mysqli_query($handle,$query));
          $id=$_SESSION['id'];

          if ($id==-1) {
            $permitted=1;
          }
          elseif($line["id_user"] != $id) {
            $permitted=0;
          }
          elseif ($line["id_user"]==$id) {
            $permitted=1;
          }
          $id_user=$line['id_user'];
          $sql="SELECT * FROM users WHERE id='$id_user'";
          $user=mysqli_fetch_array(mysqli_query($handle,$sql));
          $sql2="SELECT * FROM popularity WHERE id_veille='$id_veille'";
          $nbv=mysqli_query($handle,$sql2)->num_rows;
          $sqli="SELECT *, DATE_FORMAT(date, '%d/%m à %H:%i') as date_format FROM comment WHERE id_veille='$id_veille' ORDER BY date DESC";
          $result=mysqli_query($handle,$sqli);
          //Valeurs simplifié en variables
          $img=$user['img'];
          $title=$line['title'];
          $subject=$line['subject'];
          $date=$line['date_formatee'];
          $keyword=$line['keyword'];
          $firstname=$user['firstname'];
          $name=$user['name'];
          $comment=$req['content'];

          echo "<h3>".ucfirst(strtolower($title)). "</h3>";

          if($line['selected']==1) {
          echo "<div class='row'>";
          echo "Veille présentée";
          echo "</div>";
          }
          echo "<div='row'>";
          echo "<div class='col-xs-8'>";
          echo "<a href ='membre.php?id=".$id_user."'><img class='personne' src='uploads/".$img."'><a>";
          echo "<a href ='membre.php?id=".$id_user."' class='nom'>".$firstname." ".$name. "</a>";
          echo "</div>";
          if ($permitted==1){
            echo "<div class='col-xs-4'>";
            echo "<a href='delete_veille.php?id=".$line['id']."'><img class='delete' src='img/delete.png'> Supprimer </a>";
            echo "<a href='update_veille.php?id=".$line['id']."'>  <img class='update' src='img/update.png'> Modifier </a>";
            echo "</div>";
          }
          echo "</div>";
          echo "</div>";
          echo "<div class='row'>";
          echo "<div class='col-md-10 col-md-offset-1'>";
          echo "<div class='contenu'>";
          echo "<div class='row'>";
          echo "<div class='col-md-6 '>";
          echo "<p class='sujet'><img class='sujet_img' src='img/sujet2.png'><span class='titre'>Sujet :</span> ".ucfirst(strtolower($subject))."</p>";
          echo "<br>";
          echo "<p class='sujet'><img class='key_img' src='img/key2.png'><span class='titre'>Catégorie : </span>".$keyword."</p>";
          echo "<br>";
          echo "<p ><img class='time_img' src='img/time.png'> " .$date."</p>";
          echo "</div>";

          $query3="SELECT * FROM contents WHERE id_veille='$id_veille'";
          $result3=mysqli_fetch_array(mysqli_query($handle,$query3));
          $type=$result3['type'];
          $ndf=$result3['nom'];
          $description=$result3['description'];


          if (!empty($type)&&!empty($ndf)){

            echo "<div class='col-md-6'>";
            echo "<a href='uploads/".$ndf."'>";
            echo "<img class='ext_img' src='img/".$type.".png'>  ";
            echo $ndf;
            echo "</a>";
            echo "<br>";
            echo "<br>";
          }
          if (!empty($description)) {
            echo "<img class='desc_img' src='img/describe.png'>";
            echo $description ;
            echo "<br><br>";
          }
          echo "<div>";
          echo "</div>";
          echo "</div>";
          echo "</div>";
        
          echo "<div class='row'>";
          echo "<div class='col-md-12'>";
          if ($logged==1){
            ?>
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_inline_share_toolbox"></div>
            <?php
          }
          if($logged == 1){
            if ($permitted==0 || $id==-1) {
              echo "<a href='like.php?id=".$line['id']."'><img src='img/heart.png'> </a>...  <span class='vote'>" .$nbv."</span>";

            }else{
              echo "<img src='img/heart.png'> ...  <span class='vote'>" .$nbv."</span>";
            }

            echo "<form action='comment.php?id=".$id_veille."' method='post'>";
            echo "<label for='comment'> Laisser un commentaire </label>";
            echo "<textarea class='form-control' name='comment'></textarea>";
            echo "<input  type='submit'>";
            echo "</form>";

          }
        } else {
          header('Location:index.php');
        }

        echo "</div>";
        echo "</div>";
        echo "<br>";
        echo "<br>";
        echo "<div class='row'>";
        echo "<div class='col-md-12'>";
        if ($result->num_rows > 0){
           echo "<label>Commentaires</label>";
           echo "<div class='com'>";
              while($com=mysqli_fetch_array($result)) {
                $id_pub=$com['id_user'];
                $sqlu="SELECT * FROM users WHERE id='$id_pub'";
                $pub=mysqli_fetch_array(mysqli_query($handle,$sqlu));
                echo "<div class='row'>";
                echo "<div class='col-xs-3'>";
                echo "<span class='titre1'>".$pub['firstname']. " " .$pub['name'];
                echo "<span class='dark'><br>".$com['date_format']. " </span></span>";
                echo "</div>";
                echo "<div class='col-xs-9'>";
                echo "<div class='com1'>".$com['content']."</div>";
                echo "</div>";
                echo "</div>";
                echo "<br>";

            }
         }

        ?>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57c82f3348ec0ccb"></script>

<?php include 'template/footer.php'; ?>
