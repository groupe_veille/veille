<?php
include ('core/database.php');
include ('core/logged.php');
?>
<!doctype html>
<html>
<head>
	<title>Inscription</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="template/style.css">
<?php include 'template/header.php';
if ($logged==1) {
      header('Location:index.php');
}

if(isset($_POST["submit"])){

   $username=htmlentities(strip_tags($_POST["username"]));
   $password=strip_tags($_POST["password"]);
   $name=strip_tags($_POST["name"]);
   $f_name=strip_tags($_POST["f_name"]);
   $promo=strip_tags($_POST["promo"]);
   $repeatpassword=strip_tags($_POST["repeatpassword"]);


   if($username&&$password&&$repeatpassword&&$promo&&$name&&$f_name) {
      if ($password==$repeatpassword){
         $password=sha1($password);
         if (isset($_FILES['img']) && $_FILES['img']['error'] == 0) {
               if ($_FILES['img']['size'] <= 5000000) {
                  $info= pathinfo($_FILES['img']['name']);
                  $extension_info=$info['extension'];
                  $extensions=array('jpg', 'jpeg', 'png');
                  if (in_array($extension_info, $extensions)) {
                     $img=basename($_FILES['img']['tmp_name']).".".$extension_info;
                     move_uploaded_file($_FILES['img']['tmp_name'], 'uploads/'.$img);
                     $query="SELECT * FROM users WHERE username= '$username'";
                     $result = mysqli_query($handle,$query);
                     if($handle->affected_rows > 0) {
                           echo "Ce pseudo n'est pas disponible";
                           } else {
                        $query="INSERT INTO `users` (name,firstname,username,password,promo,img) VALUES ('$name','$f_name','$username','$password','$promo','$img')";
                        $result = mysqli_query($handle,$query);

                        die("Inscription terminée <a href='login.php'>connectez vous</a>");

                        }
                  } else {
                     echo "Format de fichier non pris en compte";
                     }
               } else {
                  echo "La taille du fichier est trop importante";
                  }
         } else {
            $query="SELECT * FROM users WHERE username= '$username'";
            $result = mysqli_query($handle,$query);
            if($handle->affected_rows > 0) {
               echo "Ce pseudo n'est pas disponible";
               } else {
                  $img="default-thumbnail.png";
                  $query="INSERT INTO `users` (name,firstname,username,password,promo,img) VALUES ('$name','$f_name','$username','$password','$promo','$img')";
                  $result = mysqli_query($handle,$query);

                  die("Inscription terminée <a href='login.php'>connectez vous</a>");
                  }
            }
      } else {
         echo "Les deux passwords doivent être identiques";
         }
   } else {
   echo "Veuillez saisir tous les champs";
      }
}

?>

<div class="container">
	<h3>Inscription</h3>
	<form method="POST" action="register.php" enctype="multipart/form-data">
		<div class="form-group">
			<label for="username">Pseudo</label>
			<input type="text" class="form-control" tabindex="1" name="username" placeholder="Votre identifiant de connexion">
		</div>
		<div class="form-group">
			<label for="name">Nom</label>
			<input type="text" class="form-control" tabindex="2" name="name" placeholder="Votre nom">
		</div>
	  <div class="form-group">
	    <label for="f_name">Prénom</label>
	    <input type="text" class="form-control" tabindex="3" name="f_name" placeholder="Votre prénom">
	  </div>
		<div class="form-group">
		<label>Promotion</label>
		<select name="promo" tabindex="4" class="form-control">
			<option value="Ada Lovelace">Ada Lovelace</option>
			<option value="Alan Turing" selected>Alan Turing</option>
			<option value="Autre">Autre</option>
		</select>
	</div>
	  <div class="form-group">
	    <label for="password">Mot de passe</label>
	    <input type="password" class="form-control" tabindex="5" name="password" placeholder="Mot de passe">
	  </div>
		<div class="form-group">
			<label for="repeatpassword">Confirmer le mot de passe</label>
			<input type="password" class="form-control" tabindex="6" name="repeatpassword" placeholder="Confirmer le mot de passe">
		</div>
	   <div class="form-group">
         <label for="img">Photo :</label>
         <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
         <input class="champ" tabindex="7" type="file" name="img"><br>
      </div>
	  <button type="submit" tabindex="8" name ="submit" class="btn btn-info">S'inscrire</button>
	</form>



</div>
<?php include ('template/footer.php'); ?>
