<?php
    include 'core/session.php';
    include 'core/database.php';
    include 'core/logged.php';
?>
<!doctype html>
<html>
<head>
	<title>Modifications</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="template/style.css">
<?php include('template/header.php'); ?>
<div class="container">
  <?php
   $id_veille=$_GET['id'];
   $query="SELECT * FROM veille WHERE id='$id_veille'";
   $line=mysqli_fetch_array(mysqli_query($handle,$query)) or die(error);

   $subject=$line['subject'];
   $subject=$line['subject'];
   $title=$line['title'];
   $keyword=$line['keyword'];

   $req="SELECT * FROM contents WHERE id='$id_veille'";
   $mfa_r=mysqli_fetch_array(mysqli_query($handle,$req));

   $ndf_x=$mfa_r['nom'];
   $type_x=$mfa_r['type'];
   $description_x=$mfa_r['description'];
   $keep=TRUE;
   if (isset($_POST['delete']))
      $keep=FALSE;

   function keys($keyword,$i) {
      if($keyword=="Web" && $i==1){
         return "selected";
      }
      if($keyword=="Objets connecté" && $i==2){
         return "selected";
      }
      if($keyword=="Logiciel" && $i==3){
         return "selected";
      }
      if($keyword=="Hacks" && $i==4){
         return "selected";
      }
      if($keyword=="Robotique" && $i==5){
         return "selected";
      }
      if($keyword=="Autres" && $i==6){
         return "selected";
      }
   }

  if ($id==$line['id_user'] || $id==-1) {
     if (isset($_POST["submit"])) {
         $id=$_GET['id'];
         $description=$_POST['description'];
         $subject=strip_tags($_POST["subject"]);
         $title=strip_tags($_POST["title"]);

         if (isset($_POST['keyword'])) {
            $keyword=$_POST["keyword"];
         } else {
            $keyword=$line['keyword'];
         }

        if ($subject&&$title&&$keyword) {
            if (isset($_FILES['content'])) {
               if ($_FILES['content']['error'] == 0) {
                  if ($_FILES['content']['size'] <= 10000000) {
                     $info= pathinfo($_FILES['content']['name']);
                     $type=$info['extension'];
                     $extensions=array('jpg', 'jpeg', 'png', 'odt', 'txt', 'md', 'pdf', 'html', 'mp4','docx', 'doc');
                     if (in_array($type, $extensions)) {
                        $content=basename($_FILES['content']['tmp_name']).".".$type;
                        move_uploaded_file($_FILES['content']['tmp_name'], 'uploads/'.$content);
                        $keep=FALSE;
                     } else {
                        echo "<p class='error'>* Format de fichier non pris en charge</p>";
                        }
                  } else {
                     echo "<p class='error'>* La taille du fichier est trop importante</p>";
                     }
                 } else {
                     echo "<p class='error'>*  Une erreur est survenue, veuillez recommencer...</p>";
                 }
               }
               if ($keep==FALSE) {
                 $sqli="DELETE FROM contents WHERE id='$id'";
               }
               if (empty(!$ndf_x) && $keep==TRUE) {
               	$sql="UPDATE veille SET subject=\"$subject\", title=\"$title\", keyword='$keyword' WHERE `id`='$id';
						          INSERT INTO contents (id,id_veille,nom,type,description)
                      VALUES ('$id','$id','$ndf_x','$type_x','$description')
                      ON DUPLICATE KEY UPDATE nom='$ndf_x', type='$type_x', description='$description'";
               } else {
						      $sql="UPDATE veille SET subject=\"$subject\", title=\"$title\", keyword='$keyword' WHERE `id`='$id';
						            INSERT INTO contents (id,id_veille,nom,type,description)
                        VALUES ('$id','$id','$content','$type','$description')
                        ON DUPLICATE KEY UPDATE nom='$content', type='$type', description='$description'";
                }

                  $req=mysqli_multi_query($handle,$sql) or die(error);
                  header('Location:membre.php');

           } else {
               echo "<p class='error'>* Veuillez renseigner tout les champs </p>";
           }
      }
   } else {
      header('Location:index.php');
   }
?>

<img class="modif" src="modif.png" alt="" />
		 <h3 class="col-xs-8">Vous pouvez modifier votre veille</h3><a href="veille.php?id=<?php echo $_GET['id'] ?>">Retour</a>

     <form method="POST" action="update_veille.php?id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data">
		   <div class="form-group">
            <label for="subject">Modifier le sujet </label>
			   <input type="text" class="form-control" tabindex="1" name="subject" placeholder="Sujet..." value="<?php echo $subject ?>">
		   </div>
		   <div class="form-group">
            <label for="title">Modifier le titre </label>
			   <input type="text" class="form-control" tabindex="2" name="title" placeholder="Titre..." value="<?php echo $title ?>">
		   </div>
		   <div class="form-group">
            <label>Catégories</label>
		         <select name="keyword" tabindex="3" class="form-control">
			         <option value="Web" <?php echo keys($keyword,1) ?>>Web</option>
			         <option value="Objets connecté" <?php echo keys($keyword,2) ?>>Objets connecté</option>
			         <option value="Logiciel" <?php echo keys($keyword,3) ?>>Logiciel</option>
			         <option value="Hacks" <?php echo keys($keyword,4) ?>>Hacks</option>
			         <option value="Robotique" <?php echo keys($keyword,5) ?>>Robotique</option>
			         <option value="Autres" <?php echo keys($keyword,6) ?>>Autres</option>
		         </select>
		   </div>
		   <?php
		             if (!empty($ndf_x)){
?>

		   <div class="form-group">
				 <label for="content">Modifier le contenu</label>
            <input class="champ" tabindex="4" type="file" name="content"><br>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="delete"> Supprimer le fichier ?
              </label>
          </div>
            <?php
            echo "<a href='uploads/".$ndf_x."'>";
            echo "<img class='ext_img' src='img/".$type_x.".png'>  ";
            echo $ndf_x;
            echo "</a>";
            echo "<br>";
            echo "<br>";
          } else {

		          ?>
		   <div class="form-group">
				 <label for="content">Contenu</label>
            <input class="champ" tabindex="4" type="file" name="content"><br>
            <?php
            }
            ?>
				<label for="description">Description</label>
				<textarea type='text' class='form-control' name="description"><?php echo $description_x ?></textarea>

		   </div>
		   <button type="submit" tabindex="5" name ="submit" class="btn btn-info">Enregistrer les modifications</button>
	   </form>
	</div>

<?php include ('template/footer.php'); ?>
