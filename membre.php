<?php
include ('core/session.php');
include ('core/database.php');
include ('core/permission.php');
include ('core/logged.php');

$q="SELECT
(SELECT count(keyword) FROM veille WHERE keyword='Web' AND id_user='$id' GROUP BY keyword) AS Web,
(SELECT count(keyword) FROM veille WHERE keyword='Hacks' AND id_user='$id' GROUP BY keyword) AS Hack,
(SELECT count(keyword) FROM veille WHERE keyword='Logiciel' AND id_user='$id' GROUP BY keyword) AS Logiciel,
(SELECT count(keyword) FROM veille WHERE keyword='Autres' AND id_user='$id' GROUP BY keyword) AS Autre,
(SELECT count(keyword) FROM veille WHERE keyword='Objets connecté' AND id_user='$id' GROUP BY keyword) AS IOT,
(SELECT count(keyword) FROM veille WHERE keyword='Robotique' AND id_user='$id' GROUP BY keyword) AS Robot";

$stats=mysqli_fetch_assoc(mysqli_query($handle,$q));
?>
<!doctype html>
<html>
<head>
  <title>Page membre</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="template/style.css">
  <script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script type="text/javascript">

window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer", {
		title:{
			text: "Catégories"
		},
		data: [
		   {
			   type: "doughnut",
			   dataPoints: [
			   	<?php
			   	// For each type ($key) of veille if number is > 0, print it in the chart
			   	foreach($stats as $key => $line)
                  	echo ($line>0)?'{label:"'.$key.'",y:'.$line.'},':'';
				   ?>

			   ]
		   }
		]
	});
	chart.render();
}
</script>
   <style>
    form {
      padding:20px 0 0 0;
      margin:0;
    }
    .cancel {
      padding:0;
    }
   </style>
<?php include('template/header.php'); ?>

<div class="container">
<div id="espace">
  <div class="modifier">
    <div class="row">
      <div class=" col-xs-12 col-md-8 col-md-offset-2">

<?php

     $query = "SELECT * FROM users WHERE id='$id'";
     $result = mysqli_query($handle,$query);

     if($handle->affected_rows > 0) {
       $line=mysqli_fetch_array($result);
       $username=$line["username"];
       $name=$line["name"];
       $f_name=$line["firstname"];
       $promo=$line["promo"];
       $id=$line["id"];
       $pseudo=$line['username'];

       $query = "SELECT * FROM veille WHERE id_user='$id'";
       $result = mysqli_query($handle,$query);
       $nbv=$result->num_rows;

       $sql="SELECT count(*) AS nb FROM users u
            INNER JOIN veille v ON u.id=v.id_user
            INNER JOIN popularity p ON v.id=p.id_veille
            WHERE u.id='$id'";
       $nb=mysqli_fetch_array(mysqli_query($handle,$sql));
       $vote=$nb['nb'];

       $sqli="SELECT * FROM badge WHERE id_user='$id'";
       $badge=mysqli_fetch_array(mysqli_query($handle,$sqli));

       if ($permitted==1) {
?>
         <div class="row">
           <div class="col-xs-12">
             <div class="row">

            <div class="col-xs-3">
               <h3> Mon profil </h3>
            </div>
            <div class="col-xs-9">
               <h3><span class='light'><a href='update_profil.php'>Modifier</a> - <a href='delete_profil.php?id=<?php echo $id?>'>Supprimer</a></span></h3>
            </div>

          </div>

        </div>
         </div>
<?php
          }
          if ($permitted==0) {
            echo "<h3>".$username."</h3>";

          }
?>

         </div>
      </div>
    </div>
    <div id="profil">
      <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
          <div class="row">
            <div class="col-xs-3">
              <?php
              echo " <img class='thumbnail' src='uploads/".$line["img"]."'>";
              echo "<br>";
              echo "<br>";


              if ($permitted==1) {
                echo "\t\t\t<a href='create_veille.php'><button type='button' class='btn btn-primary'>Ajouter une veille</button></a>\n";
              }

              echo"</div>";
              echo"<div class='col-xs-4'>\n";

              echo "\t\t\t\t<ul>\n";
              echo "\t\t\t\t\t<li class='fat'>".ucfirst(strtolower($f_name))." ".ucfirst(strtolower($name))."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Promo :</span> ".$promo."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Pseudo : </span>".$pseudo."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Veilles postées : </span>".$nbv."</li>\n";
              echo "\t\t\t\t\t<li><span class='light'>Votes recu : </span>".$vote."</li>\n";

              if($badge['boss']==1)
              echo "\t\t\t\t\t<li>BOSS</li>\n";

              if($badge['veille']==1)
              echo "\t\t\t\t\t<li>Tu passes en veille!</li>\n";

              echo "\t\t\t\t</ul>\n";
              echo "\t\t\t</div>\n";
              echo "\t\t<div class='col-xs-5'>\n";
            }


            ?>
            <div id="chartContainer" style="height: 200px; width: 40  %;"></div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <div id="content">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
         <div class="row">
            <div class="col-xs-8">
               <h3> Toutes les veilles </h3>
            </div>
            <div class="col-xs-3">
<?php
      $query="SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee  FROM ( SELECT * FROM veille ORDER BY date desc ) v WHERE v.id_user='$id'";
      $result=mysqli_query($handle,$query);
      include ('include/search.php');

      echo "\t\t\t\t<form action='membre.php' method='get'>\n";
      if(isset($_GET['id'])) {
         echo "\t\t\t\t\t<input type='hidden' name='id' value='".$id."'>\n";
      }
      echo "\t\t\t\t\t<input autocomplete='off' class='form-control' tabindex='1' name='search' placeholder='Rechercher' title='Rechercher dans Veille' value='".$s."'>";

      echo "\t\t\t\t</form>\n";

      echo "\t\t\t</div>\n";
      echo "\t\t\t<div class='col-xs-1 cancel'>\n";
      if(isset($_GET['search'])) {
         if(isset($_GET['id'])) {
            echo "<a class='cancel' href='membre.php?id=".$id."'>X</a>\n";
         } else {
            echo "<a class='cancel' href='membre.php'>X</a>\n";
         }
      }
      echo "\t\t\t</div>\n";
      echo "\t\t</div>\n";


      if($handle->affected_rows > 0) {
         while($line=mysqli_fetch_array($result)) {

            echo "\t\t<div id='veille_membre'>\n";
            echo "\t\t\t<div class='row'>\n";
            echo "\t\t\t\t<div class='col-xs-2'>\n";
            echo "\t\t\t\t\t<p class='dark'>" .$line['date_formatee']."</p>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-7'>\n";
            echo "\t\t\t\t\t<div class='subject'>\n";

            $title = $line['subject'];
            if(strlen($title) > 60){
               $title = substr($title, 0, 60) ."...";
            }
            echo "\t\t\t\t\t\t<p class='titre'> <a href='veille.php?id=".$line['id']."'><img class='sujet_img' src='img/sujet.png'>   " . $title." </a></p>\n";
            echo "\t\t\t\t\t\t<p> <img class='key_img' src='img/key.png'> ".$line['keyword']."</p>\n";
            echo "\t\t\t\t\t</div>\n";
            echo "\t\t\t\t</div>\n";

            if ($permitted==1){
               echo "\t\t\t\t<div class='col-xs-1'>\n";
               echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='img/go.png'></a>\n";
               echo "\t\t\t\t</div>\n";
               echo "\t\t\t\t<div class='col-xs-1'>\n";
               echo "\t\t\t\t\t<a href='delete_veille.php?id=".$line['id']."'><img class='delete' src='img/delete.png'></a>\n";
               echo "\t\t\t\t</div>\n";
               echo "\t\t\t\t<div class='col-xs-1'>\n";
               echo "\t\t\t\t\t<a href='update_veille.php?id=".$line['id']."'><img class='update' src='img/update.png'></a>\n";
               echo "\t\t\t\t</div>\n";
            } elseif ($permitted==0) {
               echo "\t\t\t\t<div class='col-xs-2 col-xs-offset-1'>\n";
               echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='img/go.png'></a>\n";
               echo "\t\t\t\t</div>\n";
            }
            echo "\t\t\t</div>\n";
            echo "\t\t</div>\n";
         }
      } else {
         echo "\t\t\t<p>Aucune veille n'a été postée pour le moment...</p>\n";
      }

      ?>
       </div>
     </div>
   </div>
  </div>
 </div>
<?php include ('template/footer.php'); ?>
