<?php
    include 'core/session.php';
    include 'core/database.php';
    include 'core/logged.php';
?>
<!doctype html>
<html>
<head>
  <title>Filtre de date</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="template/style.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
  <style>
      select {
         margin-bottom:20px;
      }
  </style>
<?php include('template/header.php'); ?>
  <div class="container-fluid">
    <div class="row">
    <div class="col-xs-8 col-xs-offset-2">
      <div class="row">
         <h3>Date  </h3>
      </div>
      <div class="row ">
         <form action="datefilter.php" method="POST">
            <div class="col-xs-5">
               <select name="month" tabindex="1" class="form-control">
                 <option value="01">Janvier</option>
                 <option value="02">Fevrier</option>
                 <option value="03">Mars</option>
                 <option value="04">Avril</option>
                 <option value="05">Mai</option>
                 <option value="06">Juin</option>
                 <option value="07">Juillet</option>
                 <option value="08">Aout</option>
                 <option value="09">Septembre</option>
                 <option value="10">Octobre</option>
                 <option value="11">Novembre</option>
                 <option value="12">Decembre</option>           
               </select>
            </div>
            <div class=" col-xs-3">
               <script>
                 $( function() {
                   $( "#datepicker" ).datepicker({
                     changeYear: true,
                     altField: "#alternate",
                     altFormat: "yy-mm-dd"
                   });
                 } );
               </script>
               <input type="text" id="datepicker" class="from-control">
               <input type="hidden" id="alternate" name="day">
            </div>
            <div class=" col-xs-2">
              <input type="submit" tabindex="2" name="submit" class="btn btn-info" value="Sélectionner">
            </div>
            <div class="col-xs-2 ">
              <input type="submit" tabindex="3" name="cancel" class="btn btn-info" value="Reset filter">
            </div>
         </form>
       </div>
       <div class="row">
       
<?php
            $query="SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee
                    FROM veille 
                    INNER JOIN users ON veille.id_user=users.id
                    ORDER BY date DESC LIMIT 25";
                    
        $month=$_POST['month'];
        $day=$_POST['day'];
        
        if(isset($day) && $day!='' && $_POST['submit']=="Sélectionner") {
            $query="SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee
                    FROM veille 
                    INNER JOIN users ON veille.id_user=users.id
                    WHERE date LIKE '$day%'
                    ORDER BY date DESC LIMIT 25";
        } elseif(isset($month) && $_POST['submit']=="Sélectionner") {
            $query="SELECT *, DATE_FORMAT(date, '%d-%m-%Y') as date_formatee
                    FROM veille 
                    INNER JOIN users ON veille.id_user=users.id
                    WHERE MONTH(date)='$month'
                    ORDER BY date DESC LIMIT 25";
        }
        if(isset($_POST['cancel'])) {
            header('Location:datefilter.php');
        }

        $result = mysqli_query($handle,$query);
        if($handle->affected_rows > 0) {
          while($line=mysqli_fetch_array($result)) {
            $i--;

            echo "\t\t<div id='veille_membre'>\n";
            echo "\t\t\t<div class='row'>\n";
            echo "\t\t\t\t<div class='col-xs-2'>\n";
            echo "\t\t\t\t\t<p class='dark'>" .$line['date_formatee']."</p>\n";
            echo "\t\t\t\t\t<a href='membre.php?id=".$line['id_user']."' class='dark'>" .$line['firstname']." ".$line['name']."</a>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-7'>\n";
            echo "\t\t\t\t\t<div class='subject'>\n";
            $title = $line['subject'];
            if(strlen($title) > 60){
              $title = substr($title, 0, 60) ."...";
            }
            echo "\t\t\t\t\t\t<p class='titre'> <a href='veille.php?id=".$line['id']."'><img class='sujet_img' src='img/sujet.png'>   " . $title." </a></p>\n";
            echo "\t\t\t\t\t\t<p> <img class='key_img' src='img/key.png'> ".$line['keyword']."</p>\n";
            echo "\t\t\t\t\t</div>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t\t<div class='col-xs-2 col-xs-offset-1'>\n";
            echo "\t\t\t\t\t<a href='veille.php?id=".$line['id']."'><img class='go' src='img/go.png'></a>\n";
            echo "\t\t\t\t</div>\n";
            echo "\t\t\t</div>\n";
            echo "\t\t</div>\n";
          }
          }
?>
  </div>
</div>
<?php include ('template/footer.php'); ?>
