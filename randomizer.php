<?php
include 'core/session.php';
include 'core/database.php';
include 'core/logged.php';
?>

<!doctype html>
<html>
<head>
  <title>Randomizer</title>
  <meta charset>
  <link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="template/style.css">

  <script language=javascript>


  /*setTimeout("afficheTexte()",5000);

  function afficheTexte()
  {
  document.getElementById('changer').style.display="inline";
}

setTimeout("nonetext()",5000);

function nonetext(){
document.getElementById('wait').style.visibility='hidden';
document.getElementById('plouf').style.display="inline";

}

//setTimeout("plouf()",5000);
//function plouf(){
//  document.getElementById('image').style.display="inline";

//}


</script>
<?php include('template/header.php');
$query = "SELECT * FROM users ";
$result = mysqli_query($handle,$query);

?>
<script type="text/javascript">
var users = [
  <?php
  //tant qu'on a un résultat ( un utilisateur )
  while($line=mysqli_fetch_array($result)){
    echo "'" . ucfirst(strtolower($line['firstname'])). " " .  ucfirst(strtolower($line['name'] )). "',";
  }
  ?>
];

//var selected_user = users [Math.floor(Math.random() * users.length)];




function select_user(cas) {

  if (cas=="veille"){
    document.getElementById('gif').style.display='block';
    document.getElementById('name').innerHTML="";

    setTimeout( function(){

      document.getElementById('gif').style.display='none';
      document.getElementById('name').innerHTML="And the winner is...<br> <span class='winner'>" + users [Math.floor(Math.random() * users.length)] + "</span>";


    } , 3000
  );
}
if (cas=="popoterie"){
document.getElementById('gif2').style.display='inline';
document.getElementById('gif3').style.display='inline';
document.getElementById('gif4').style.display='inline';
document.getElementById('gif5').style.display='inline';

document.getElementById('task1').innerHTML="";
document.getElementById('task2').innerHTML="";
document.getElementById('task3').innerHTML="";
document.getElementById('task4').innerHTML="";



setTimeout( function(){
document.getElementById('gif2').style.display='none';
document.getElementById('gif3').style.display='none';
document.getElementById('gif4').style.display='none';
document.getElementById('gif5').style.display='none';


document.getElementById('task1').innerHTML=users [Math.floor(Math.random() * users.length)];
document.getElementById('task2').innerHTML=users [Math.floor(Math.random() * users.length)];
document.getElementById('task3').innerHTML=users [Math.floor(Math.random() * users.length)];
document.getElementById('task4').innerHTML=users [Math.floor(Math.random() * users.length)];



} , 1500
);
}
/*if (cas == "popoterie") {
  for (var i = 1; i <= 4; i++) {
    document.getElementById('gif2' + i).style.display = 'inline';
    document.getElementById('task' + i).innerHTML = "";
  }


  setTimeout(function () {
    var usersTmp = users.slice(0); // on copie users dans usersTmp, users ne sert plus donc qu'à renseigner ce tableau à chaque appel de la fonction pour repartir sur un tableau d'utilisateur complet

    for (var i = 1; i <=  4; i++) {
      document.getElementById('gif2' + (i+1)).style.display = 'none';
      var randomizedIndex = Math.floor(Math.random() * usersTmp.length); //on génère un nombre aléatoire ( maximum = nombre d'élément du tableau usersTmp )
      document.getElementById('task' + i).innerHTML = usersTmp [randomizedIndex]; // on récupère l'élément du tableau qui porte ce "numéro" dans usersTmp
      usersTmp = usersTmp.slice(randomizedIndex, 1); // on supprime l'élement qu'on vient d'utiliser dans le tableau
    }
  }, 3000
);
}*/
if (cas=="popotier"){
  document.getElementById('gif6').style.display='inline';
  setTimeout( function(){
    document.getElementById('boss').style.display='inline';

    document.getElementById('gif6').style.display='none';
    document.getElementById('popotier').innerHTML= users [Math.floor(Math.random() * users.length)] + "</span>";

  } , 1500
);
}
}

</script>


<div class="container">

  <div class="row">

    <div class="col-md-6">

      <h3>Veilleur du jour</h3>

      <button class="btn btn-danger" type="button" name="button" onclick="select_user('veille')">GO</button>

      <div class="select1">

        <img id='gif' src='img/rand.gif' style="display:none">
        <h2 id="name"></h2>

      </div>
      <h3>Popotier de la semaine</h3>
      <button class="btn btn-success" type="button" name="button" onclick="select_user('popotier')">GO</button>

      <div class="select2">
        <div class="col-xs-3 col-md-2">
          <img id="boss" src='img/boss.png' style="display:none" >
          <img id='gif6' src='img/rand.gif' style="display:none">
        </div>
        <div class="col-xs-9 col-md-10">
          <h2 id="popotier"></h2>
        </div>
      </div>
    </div>



    <div class="col-md-6">



      <h3>Popoterie</h3>
      <button class="btn btn-info" type="button" name="button" onclick="select_user('popoterie')">GO</button>

      <div class="row">
        <div class="col-xs-12">

          <div class="task">
            <div class="col-xs-3">
              <img src="img/broom.png" alt="" />
            </div>
            <div class="col-xs-9">
              <img id='gif2' src='img/rand2.gif' style="display:none">

              <h2 id="task1"></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">

          <div class="task">
            <div class="col-xs-3">
              <img src="img/dust.png" alt="" />
            </div>
            <div class="col-xs-9">
              <img id='gif3' src='img/rand2.gif' style="display:none">

              <h2 id="task2"></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">

          <div class="task">
            <div class="col-xs-3">
              <img class="kitchen" src="img/kit.png" alt="" />
            </div>
            <div class="col-xs-9">
              <img id='gif4' src='img/rand2.gif' style="display:none">

              <h2 id="task3"></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">


          <div class="task">
            <div class="col-xs-3">
              <img src="img/wc.png" alt="" />
            </div>
            <div class="col-xs-9">
              <img id='gif5' src='img/rand2.gif' style="display:none">

              <h2 id="task4"></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>






<!--$keep=TRUE;
while($keep) {
$i=rand(0,100);
$query = "SELECT * FROM users WHERE id='$i'";
$result = mysqli_query($handle,$query);
if ($result->num_rows > 0) {
echo "<div class='random'>";

$line=mysqli_fetch_array($result);
echo "\t\t<p>".ucfirst(strtolower($line['firstname']))." ".ucfirst(strtolower($line['name']))."</p>\n";
echo "</div>";
$keep=FALSE;


}
}
echo "\t\t<a href='randomizer.php'><button type='button' class='btn btn-default'>Reroll</button></a>\n";

?>
</div>
-->
<?php include ('template/footer.php'); ?>
